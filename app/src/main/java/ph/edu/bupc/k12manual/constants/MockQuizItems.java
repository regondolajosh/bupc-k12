package ph.edu.bupc.k12manual.constants;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

import ph.edu.bupc.k12manual.models.QuizItem;

/**
 * Created by Sheychan on 8/21/2016.
 */
public class MockQuizItems {
    public static ArrayList<QuizItem> getQuizItems(int size) {
        ArrayList<QuizItem> items = new ArrayList<>();

        QuizItem item = new QuizItem();
        item.setQuestion("1.The tapeworm, ascaris and hookworm live in the stomach of humans. This shows _________feeding relationship.?");
        item.setChoices(Arrays.asList("Mutualism", "Predation", "Parasitism", "Competition"));
        item.setAcceptedAnswers(Arrays.asList("Parasitism"));
        items.add(item);


        item = new QuizItem();
        item.setQuestion("The bird built its nest on a tree. The tree is not harmed but the bird is benefited. What interrelationship is existing in this situation");
        item.setChoices(Arrays.asList("Mutualism", "Commensalism", "Predation", "Competition"));
        item.setAcceptedAnswers(Arrays.asList("Commensalism"));
        items.add(item);


        item = new QuizItem();
        item.setQuestion("The bird gets its food from the mouth of the crocodile and the crocodile’s teeth get cleaned. What feeding relationship is shown by the two animals?");
        item.setChoices(Arrays.asList("Mutualism", "Commensalism", "Predation", "Competition"));
        item.setAcceptedAnswers(Arrays.asList("Mutualism"));
        items.add(item);


        item = new QuizItem();
        item.setQuestion("1.Mang Artemio pulled the weeds in his garden of roses. What is his purpose?");
        item.setChoices(Arrays.asList("To avoid predation between the weeds and roses", "The weeds are parasitic to the roses", "To avoid competition between the roses and weeds", "To avoid commensalism to happen"));
        item.setAcceptedAnswers(Arrays.asList("To avoid competition between the roses and weeds"));
        items.add(item);


        item = new QuizItem();
        item.setQuestion("Higher temperatures cause higher rate of evaporation and long dry season in some areas in the world");
        item.setChoices(Arrays.asList("El Niño/extreme drought", "La Niña/heavy rainfall", "Rising of sea level", "Warmer temperature affects human health"));
        item.setAcceptedAnswers(Collections.singletonList("El Niño/extreme drought"));
        items.add(item);


        item = new QuizItem();
        item.setQuestion("Rice fields have less harvest due to long dry season or long rainy season");
        item.setChoices(Arrays.asList("La Niña/heavy rainfall", "El Niño/extreme drough", "Less food for the consumers", "Rising of sea level"));
        item.setAcceptedAnswers(Collections.singletonList("Less food for the consumers"));
        items.add(item);


        Collections.shuffle(items);
        return new ArrayList<>(items.subList(0, size));
    }
}

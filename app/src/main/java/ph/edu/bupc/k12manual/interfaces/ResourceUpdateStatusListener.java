package ph.edu.bupc.k12manual.interfaces;

import android.view.View;

public interface ResourceUpdateStatusListener {
    View getHandler();

    void onUpdateCompleted();
}

package ph.edu.bupc.k12manual.constants;


public class BundleIDs {

    public static final String TOPIC = "topic";
    public static final String IMAGE_URL = "image_url";
    public static final String QUIZ_ITEM = "quiz_item";
    public static final String QUIZ_ITEMS = "quiz_items";
    public static final String ANSWERS = "answers";
    public static final String VIDEO_FILE = "video_file";
    public static final String ACTIVITY_LAYOUT = "activity_layout";

    private BundleIDs() {
    }
}

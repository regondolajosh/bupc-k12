package ph.edu.bupc.k12manual.interfaces;

import android.support.design.widget.TabLayout;
import android.support.v7.widget.SearchView;

import java.util.List;


public interface MainViewController {

    TabLayout getTabLayout();

    List<SearchView.OnQueryTextListener> getQueryTextListeners();
}

package ph.edu.bupc.k12manual.activities;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.widget.FrameLayout;

import java.util.ArrayList;
import java.util.concurrent.BrokenBarrierException;

import ph.edu.bulsu.k12manual.R;
import ph.edu.bupc.k12manual.constants.MockQuizItems;
import ph.edu.bupc.k12manual.constants.Tlk1QuizItems;
import ph.edu.bupc.k12manual.constants.Tlk2QuizItems;
import ph.edu.bupc.k12manual.constants.Tlk3QuizItems;
import ph.edu.bupc.k12manual.constants.Tlk4QuizItems;
import ph.edu.bupc.k12manual.constants.Tlk5QuizItems;
import ph.edu.bupc.k12manual.constants.Tlk6QuizItems;
import ph.edu.bupc.k12manual.fragments.QuizItemFragment;
import ph.edu.bupc.k12manual.fragments.QuizResultFragment;
import ph.edu.bupc.k12manual.fragments.QuizStartupFragment;
import ph.edu.bupc.k12manual.interfaces.QuizViewController;
import ph.edu.bupc.k12manual.models.QuizItem;
import ph.edu.bupc.k12manual.models.Topic;

public class QuizActivity extends HidingToolbarActivity implements QuizViewController {

    private int DEFAULT_QUIZ_QUESTION_ITEMS_SIZE = 10;

    private ArrayList<QuizItem> items;
    private FrameLayout flMain;
    private ArrayList<QuizItemFragment> quizItemFragments;
    private ArrayList<String> answers;

    private int currentQuizFragmentPosition;

    private int totalPoints = 0;

    @Override
    protected int getContentViewId() {
        return R.layout.activity_quiz;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setToolbarTitle("Quiz");

        useFragment(QuizStartupFragment.newInstance());
    }


    private void useFragment(Fragment fragment) {
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left);
        fragmentTransaction.replace(R.id.flMain, fragment, null);
        fragmentTransaction.commitAllowingStateLoss();
    }

    @Override
    public void nextQuiz() {
        currentQuizFragmentPosition++;
        if (currentQuizFragmentPosition < items.size())
            useFragment(quizItemFragments.get(currentQuizFragmentPosition));
        else
            showResults();

    }

    @Override
    public void startQuiz() {

    }

    @Override
    public void answer(String choice) {
        if (currentQuizFragmentPosition < items.size()) {
            answers.add(choice);
            nextQuiz();
        } else {
            showResults();
        }
    }

    @Override
    public void showResults() {
        useFragment(QuizResultFragment.newInstance(items, answers));
    }

    @Override
    public void onTlk1Quiz() {
        selectQuiz(1);
        currentQuizFragmentPosition = 0;
        useFragment(quizItemFragments.get(currentQuizFragmentPosition));
    }

    @Override
    public void onTlk2Quiz() {
        selectQuiz(2);
        currentQuizFragmentPosition = 0;
        useFragment(quizItemFragments.get(currentQuizFragmentPosition));
    }

    @Override
    public void onTlk3Quiz() {
        selectQuiz(3);
        currentQuizFragmentPosition = 0;
        useFragment(quizItemFragments.get(currentQuizFragmentPosition));
    }

    @Override
    public void onTlk4Quiz() {
        selectQuiz(4);
        currentQuizFragmentPosition = 0;
        useFragment(quizItemFragments.get(currentQuizFragmentPosition));
    }

    @Override
    public void onTlk5Quiz() {
        selectQuiz(5);
        currentQuizFragmentPosition = 0;
        useFragment(quizItemFragments.get(currentQuizFragmentPosition));

    }

    @Override
    public void onTlk6Quiz() {
        selectQuiz(6);
        currentQuizFragmentPosition = 0;
        useFragment(quizItemFragments.get(currentQuizFragmentPosition));

    }

    public void selectQuiz(int quizTopic) {
        switch (quizTopic) {
            case 1:
                items = Tlk1QuizItems.getQuizItems(DEFAULT_QUIZ_QUESTION_ITEMS_SIZE);
                break;
            case 2:
                items = Tlk2QuizItems.getQuizItems(DEFAULT_QUIZ_QUESTION_ITEMS_SIZE);
                break;
            case 3:
                items = Tlk3QuizItems.getQuizItems(DEFAULT_QUIZ_QUESTION_ITEMS_SIZE);
                break;
            case 4:
                items = Tlk4QuizItems.getQuizItems(DEFAULT_QUIZ_QUESTION_ITEMS_SIZE);
                break;
            case 5:
                items = Tlk5QuizItems.getQuizItems(DEFAULT_QUIZ_QUESTION_ITEMS_SIZE);
                break;
            case 6:
                items = Tlk6QuizItems.getQuizItems(DEFAULT_QUIZ_QUESTION_ITEMS_SIZE);
                break;

            default:
                items = Tlk1QuizItems.getQuizItems(DEFAULT_QUIZ_QUESTION_ITEMS_SIZE);
        }

        answers = new ArrayList<>();
        quizItemFragments = new ArrayList<>();

        for (QuizItem item : items) {
            quizItemFragments.add(QuizItemFragment.newInstance(item));
        }
    }
}

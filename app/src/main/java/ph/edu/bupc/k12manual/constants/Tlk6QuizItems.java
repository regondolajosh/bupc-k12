package ph.edu.bupc.k12manual.constants;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

import ph.edu.bupc.k12manual.models.QuizItem;


public class Tlk6QuizItems {

    public static ArrayList<QuizItem> getQuizItems(int size) {
        ArrayList<QuizItem> items = new ArrayList<>();
        QuizItem item = new QuizItem();
        item.setQuestion("This is polluted precipitation. When fuels such as firewood, gasoline, and other fossil fuels used in motor vehicles are burned, carbon dioxide, sulfur dioxide, and nitrous oxide are released into the air and reached the clouds. Combined with water vapor, these gases form harmful solutions, known as ____________________. This is the result of air pollution, a harmful environmental condition that can make the topsoil unfertile.");
        item.setChoices(Arrays.asList("Water pollution", "Air pollution", "Red tide", "Algal bloom", "Acid rain"));
        item.setAcceptedAnswers(Arrays.asList("Acid rain"));
        items.add(item);

        item = new QuizItem();
        item.setQuestion("A mixture of solid particles and gases in the air. Car emissions, chemicals from factories, dust, pollen and mold spores may be suspended as particles in the air. It is harmful to our health when inhaling dirty air.");
        item.setChoices(Arrays.asList("Water pollution", "Air pollution", "Red tide", "Algal bloom", "Acid rain"));
        item.setAcceptedAnswers(Arrays.asList("Algal bloom"));
        items.add(item);


        item = new QuizItem();
        item.setQuestion("When gardeners and farmerfs use fertilizer, crops grow healthy. However, fertilizer also causes harm to the environment. When the excess fertilizer is carried by running water to bodies of water a problem occurs. The fertilized water will cause the rapid or fast growth of small aquatic plants called _________________.");
        item.setChoices(Arrays.asList("Water pollution", "Air pollution", "Red tide", "Algal bloom", "Acid rain"));
        item.setAcceptedAnswers(Arrays.asList("Red tide"));
        items.add(item);


        item = new QuizItem();
        item.setQuestion("This is the effect of throwing wastes in the bodies of water like garbage, dead animals, and industrial chemical wastes from factories. When bodies of water contain a lot of materials to the point of decomposition cannot proceed normally.");
        item.setChoices(Arrays.asList("Water pollution", "Air pollution", "Red tide", "Algal bloom", "Acid rain"));
        item.setAcceptedAnswers(Arrays.asList("Air pollution"));
        items.add(item);

        item = new QuizItem();
        item.setQuestion("This is the result of effect of contaminated water. When water is dirty microscopic organisms called dinoflagellates multipy fast. Dinoflagellates produce poisonous substance that can be eaten by aquatic such as shellfish. Shellfish, like tahong, may not be safe eaten once it takes toxic substances. When eaten, contaminated tahong can cause vomiting, numbness, paralysis or even death.");
        item.setChoices(Arrays.asList("Water pollution", "Air pollution", "Red tide", "Algal bloom", "Acid rain"));
        item.setAcceptedAnswers(Arrays.asList("Water pollution"));
        items.add(item);

        item = new QuizItem();
        item.setQuestion("Which of the following materials contain chlorofluorocarbons or CFCs that can deplete the ozone layer?");
        item.setChoices(Arrays.asList("aerosol", "plastic bottle", "rubber band", "soap"));
        item.setAcceptedAnswers(Arrays.asList("aerosol"));
        items.add(item);

        item = new QuizItem();
        item.setQuestion("In which place can there be high methane gas?");
        item.setChoices(Arrays.asList("in a beach", "in a barber shop", "back of the house with piles of degradable wastes", "shopping malls where lots of people are breathing"));
        item.setAcceptedAnswers(Arrays.asList("back of the house with piles of degradable wastes"));
        items.add(item);

        item = new QuizItem();
        item.setQuestion("What place can produce too much nitrous oxide");
        item.setChoices(Arrays.asList("kitchen", "rice field", "church", "school"));
        item.setAcceptedAnswers(Arrays.asList("kitchen"));
        items.add(item);

        item = new QuizItem();
        item.setQuestion("When does burning wood become harmful?");
        item.setChoices(Arrays.asList("when it produces carbon dioxide", "when it gives us light", "when it gives us heat during cold days", "when it is used to cook food"));
        item.setAcceptedAnswers(Arrays.asList("when it produces carbon dioxide"));
        items.add(item);

        item = new QuizItem();
        item.setQuestion("Certain materials that can produce greenhouse gases are harmful because");
        item.setChoices(Arrays.asList("they make the world to look dark green", "they make the world very dark", "they make the world to rotate very fast", "they make the world warmer due to global warming"));
        item.setAcceptedAnswers(Arrays.asList("they make the world warmer due to global warming"));
        items.add(item);

        item = new QuizItem();
        item.setQuestion("Burning too much like woods and garbage can produce air pollutants making the air we breathe dirty. It also produces carbon dioxide that causes global warming.");
        item.setChoices(Arrays.asList("HARMFUL", "BENEFICIAL"));
        item.setAcceptedAnswers(Arrays.asList("HARMFUL"));
        items.add(item);

        item = new QuizItem();
        item.setQuestion("Eating too much beef, poultry products, fish and pork can give body too much cholesterol. Cholesterol can block the arteries and prevents blood from flowing to the heart and brain, causing heart attack and stroke.");
        item.setChoices(Arrays.asList("HARMFUL", "BENEFICIAL"));
        item.setAcceptedAnswers(Arrays.asList("HARMFUL"));
        items.add(item);

        item = new QuizItem();
        item.setQuestion("Additives are chemicals that can add delicious taste to our foods. Some of these chemicals are carcinogenic or can cause cancer. Some of them can also cause allergies.");
        item.setChoices(Arrays.asList("HARMFUL", "BENEFICIAL"));
        item.setAcceptedAnswers(Arrays.asList("HARMFUL"));
        items.add(item);

        item = new QuizItem();
        item.setQuestion("Plastic materials like bags, chairs, and bottles are thrown daily. They are non-biodegradable materials so they can clog our canals and other water ways which can cause flooding during heavy rains.");
        item.setChoices(Arrays.asList("HARMFUL", "BENEFICIAL"));
        item.setAcceptedAnswers(Arrays.asList("BENEFICIAL"));
        items.add(item);

        item = new QuizItem();
        item.setQuestion("Chlorofluorocarbons (CFC’s) in form of Freon that come from refrigerator units and air conditioners can destroy the ozone layer in the atmosphere.");
        item.setChoices(Arrays.asList("HARMFUL", "BENEFICIAL"));
        item.setAcceptedAnswers(Arrays.asList("HARMFUL"));
        items.add(item);



        Collections.shuffle(items);

        return new ArrayList<>(items.subList(0, size));
    }
}

package ph.edu.bupc.k12manual.interfaces;

public interface QuizViewController {
    void nextQuiz();

    void startQuiz();

    void answer(String choice);

    void showResults();

    void onTlk1Quiz();

    void onTlk2Quiz();

    void onTlk3Quiz();

    void onTlk4Quiz();

    void onTlk5Quiz();

    void onTlk6Quiz();

}
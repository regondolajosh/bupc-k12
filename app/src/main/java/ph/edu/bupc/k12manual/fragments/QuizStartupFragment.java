package ph.edu.bupc.k12manual.fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import ph.edu.bulsu.k12manual.R;
import ph.edu.bupc.k12manual.activities.QuizActivity;
import ph.edu.bupc.k12manual.interfaces.QuizViewController;
import ph.edu.bupc.k12manual.models.Topic;

public class QuizStartupFragment extends BaseFragment implements View.OnClickListener {

    private QuizViewController quizViewController;
    private Button  btnTlk1Quiz, btnTlk2Quiz, btnTlk3Quiz, btnTlk4Quiz, btnTlk5Quiz, btnTlk6Quiz;

    public static QuizStartupFragment newInstance() {
        Bundle args = new Bundle();

        QuizStartupFragment fragment = new QuizStartupFragment();
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        quizViewController = (QuizViewController) context;
    }

    @Override
    public int getParentLayoutId() {
        return R.layout.layout_choose_quiz_topic;
    }

    @Override
    public void initializeParentView(View view) {

        btnTlk1Quiz = (Button) view.findViewById(R.id.btnTlk1Quiz);
        btnTlk2Quiz = (Button) view.findViewById(R.id.btnTlk2Quiz);
        btnTlk3Quiz = (Button) view.findViewById(R.id.btnTlk3Quiz);
        btnTlk4Quiz = (Button) view.findViewById(R.id.btnTlk4Quiz);
        btnTlk5Quiz = (Button) view.findViewById(R.id.btnTlk5Quiz);
        btnTlk6Quiz = (Button) view.findViewById(R.id.btnTlk6Quiz);

        btnTlk1Quiz.setOnClickListener(this);
        btnTlk2Quiz.setOnClickListener(this);
        btnTlk3Quiz.setOnClickListener(this);
        btnTlk4Quiz.setOnClickListener(this);
        btnTlk5Quiz.setOnClickListener(this);
        btnTlk6Quiz.setOnClickListener(this);

    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnTlk1Quiz:
                quizViewController.onTlk1Quiz();
                break;

            case R.id.btnTlk2Quiz:
                quizViewController.onTlk2Quiz();
                break;

            case R.id.btnTlk3Quiz:
                quizViewController.onTlk3Quiz();
                break;

            case R.id.btnTlk4Quiz:
                quizViewController.onTlk4Quiz();
                break;
            case R.id.btnTlk5Quiz:
                quizViewController.onTlk5Quiz();
                break;
            case R.id.btnTlk6Quiz:
                quizViewController.onTlk6Quiz();
                break;
        }
    }
}

package ph.edu.bupc.k12manual.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import ph.edu.bulsu.k12manual.R;


public class DevelopersFragment extends Fragment {

    private final String TAG = DevelopersFragment.class.getSimpleName();
    private View mView;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_developers, container, false);

        return mView;
    }

    public static DevelopersFragment newInstance() {
        
        Bundle args = new Bundle();
        
        DevelopersFragment fragment = new DevelopersFragment();
        fragment.setArguments(args);
        return fragment;
    }
}

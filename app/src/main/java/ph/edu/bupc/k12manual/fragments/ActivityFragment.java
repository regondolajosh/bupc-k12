package ph.edu.bupc.k12manual.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import ph.edu.bulsu.k12manual.R;
import ph.edu.bupc.k12manual.activities.TLKDetailsActivity;
import ph.edu.bupc.k12manual.activities.ZoomImageActivity;
import ph.edu.bupc.k12manual.constants.BundleIDs;

public class ActivityFragment extends BaseFragment implements View.OnClickListener{

    @Override
    public int getParentLayoutId() {
        return R.layout.fragment_activity;
    }

    public static ActivityFragment newInstance() {
        Bundle args = new Bundle();

        ActivityFragment fragment = new ActivityFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void initializeParentView(View view) {
        view.findViewById(R.id.btnTlk1Quiz).setOnClickListener(this);
        view.findViewById(R.id.btnTlk2Quiz).setOnClickListener(this);
        view.findViewById(R.id.btnTlk3Quiz).setOnClickListener(this);
        view.findViewById(R.id.btnTlk4Quiz).setOnClickListener(this);
        view.findViewById(R.id.btnTlk5Quiz).setOnClickListener(this);
        view.findViewById(R.id.btnTlk6Quiz).setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        Intent intent = new Intent(getActivity(), TLKDetailsActivity.class);
        switch (v.getId()) {

            case R.id.btnTlk1Quiz:
                intent.putExtra(BundleIDs.ACTIVITY_LAYOUT, 1);
                startActivity(intent);
                break;
            case R.id.btnTlk2Quiz:
                intent.putExtra(BundleIDs.ACTIVITY_LAYOUT, 2);
                startActivity(intent);
                break;
            case R.id.btnTlk3Quiz:
                intent.putExtra(BundleIDs.ACTIVITY_LAYOUT, 3);
                startActivity(intent);
                break;
            case R.id.btnTlk4Quiz:
                intent.putExtra(BundleIDs.ACTIVITY_LAYOUT, 4);
                startActivity(intent);
                break;
            case R.id.btnTlk5Quiz:
                intent.putExtra(BundleIDs.ACTIVITY_LAYOUT, 5);
                startActivity(intent);
                break;
            case R.id.btnTlk6Quiz:
                intent.putExtra(BundleIDs.ACTIVITY_LAYOUT, 6);
                startActivity(intent);
                break;



        }
    }
}

package ph.edu.bupc.k12manual;

import android.app.Application;


public class K12ManualApplication extends Application {

    private static final String TAG = K12ManualApplication.class.getSimpleName();

    private static K12ManualApplication k12ManualApplication;

    public static synchronized K12ManualApplication getInstance() {
        return k12ManualApplication;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        k12ManualApplication = this;
    }

}

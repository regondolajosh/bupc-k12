package ph.edu.bupc.k12manual.activities;

import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import ph.edu.bulsu.k12manual.R;
import ph.edu.bupc.k12manual.constants.BundleIDs;
import ph.edu.bupc.k12manual.models.Topic;

/**
 * Created by Sheychan on 6/17/2016.
 */
public class TopicContentActivity extends HidingToolbarActivity {

    private Topic topic;

    @Override

    protected int getContentViewId() {
        return getLayoutId(topic);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        topic = getIntent().getParcelableExtra(BundleIDs.TOPIC);
        super.onCreate(savedInstanceState);
        setTitle(topic.getTitle());


    }


    @Override
    protected void onResume() {
        super.onResume();
    }

    private int getLayoutId(Topic topic) {
        switch (topic.getTitle()) {
            case "TLK1":
                return R.layout.fragment_tlk_1;
            case "TLK2":
                return R.layout.fragment_tlk_2;
            case "TLK3":
                return R.layout.fragment_tlk_3;
            case "TLK4":
                return R.layout.fragment_tlk_4;
            case "TLK5":
                return R.layout.fragment_tlk_5;
            case "TLK6":
                return R.layout.fragment_tlk_6;
            default:
                return R.layout.fragment_tlk_1;
        }
    }

    public void showVideo(View view) {
        Intent intent = new Intent(this, SimulationActivity.class);
        intent.putExtra(BundleIDs.VIDEO_FILE, view.getId());
        startActivity(intent);
    }

    public void showImage(View view) {
        Intent intent = new Intent(this, ZoomImageActivity.class);
        intent.putExtra(BundleIDs.VIDEO_FILE, view.getId());
        startActivity(intent);
    }
}

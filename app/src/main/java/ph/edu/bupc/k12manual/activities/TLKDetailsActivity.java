package ph.edu.bupc.k12manual.activities;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import ph.edu.bulsu.k12manual.R;
import ph.edu.bupc.k12manual.constants.BundleIDs;

/**
 * Created by henry on 2/17/17.
 */

public class TLKDetailsActivity extends AppCompatActivity {

    private int viewId;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        viewId = getIntent().getIntExtra(BundleIDs.VIDEO_FILE, 0);
        setContentView(getLayoutId(viewId));
    }

    private int getLayoutId(int viewId) {
        switch (viewId) {
            case 1: return R.layout.fragment_act_1;
            case 2: return R.layout.fragment_act_2;
            case 3: return R.layout.fragment_act_3;
            case 4: return R.layout.fragment_act_4;
            case 5: return R.layout.fragment_act_5;
            case 6: return R.layout.fragment_act_6;
            default: return R.layout.fragment_act_1;
        }
    }
}

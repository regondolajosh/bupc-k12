package ph.edu.bupc.k12manual.constants;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

import ph.edu.bupc.k12manual.models.QuizItem;


public class Tlk2QuizItems {
    public static ArrayList<QuizItem> getQuizItems(int size) {
        ArrayList<QuizItem> items = new ArrayList<>();

        QuizItem item = new QuizItem();
        item.setQuestion("The tapeworm, ascaris and hookworm live in the stomach of humans. This shows _________feeding relationship.?");
        item.setChoices(Arrays.asList("Mutualism", "Predation", "Parasitism", "Competition"));
        item.setAcceptedAnswers(Arrays.asList("Parasitism"));
        items.add(item);


        item = new QuizItem();
        item.setQuestion("The bird built its nest on a tree. The tree is not harmed but the bird is benefited. What interrelationship is existing in this situation");
        item.setChoices(Arrays.asList("Mutualism", "Commensalism", "Predation", "Competition"));
        item.setAcceptedAnswers(Arrays.asList("Commensalism"));
        items.add(item);


        item = new QuizItem();
        item.setQuestion("The bird gets its food from the mouth of the crocodile and the crocodile’s teeth get cleaned. What feeding relationship is shown by the two animals?");
        item.setChoices(Arrays.asList("Mutualism", "Commensalism", "Predation", "Competition"));
        item.setAcceptedAnswers(Arrays.asList("Mutualism"));
        items.add(item);


        item = new QuizItem();
        item.setQuestion("Mang Artemio pulled the weeds in his garden of roses. What is his purpose?");
        item.setChoices(Arrays.asList("To avoid predation between the weeds and roses", "The weeds are parasitic to the roses", "To avoid competition between the roses and weeds", "To avoid commensalism to happen"));
        item.setAcceptedAnswers(Arrays.asList("To avoid competition between the roses and weeds"));
        items.add(item);


        item = new QuizItem();
        item.setQuestion("Higher temperatures cause higher rate of evaporation and long dry season in some areas in the world");
        item.setChoices(Arrays.asList("El Niño/extreme drought", "La Niña/heavy rainfall", "Rising of sea level", "Warmer temperature affects human health"));
        item.setAcceptedAnswers(Collections.singletonList("El Niño/extreme drought"));
        items.add(item);


        item = new QuizItem();
        item.setQuestion("Rice fields have less harvest due to long dry season or long rainy season");
        item.setChoices(Arrays.asList("La Niña/heavy rainfall", "El Niño/extreme drough", "Less food for the consumers", "Rising of sea level"));
        item.setAcceptedAnswers(Collections.singletonList("Less food for the consumers"));
        items.add(item);

        item = new QuizItem();
        item.setQuestion("Human and lice, the human is the ________ while the lice are the ectoparasites.");
        item.setChoices(Arrays.asList("PREDATION", "PARASITE", "HOST", "PREY"));
        item.setAcceptedAnswers(Collections.singletonList("HOST"));
        items.add(item);

        item = new QuizItem();
        item.setQuestion("Feeding relationship where animal kills for another animal for food like mouse and rat.");
        item.setChoices(Arrays.asList("PREDATION", "PARASITE", "PARASITISM", "PREY"));
        item.setAcceptedAnswers(Collections.singletonList("PREDATION"));
        items.add(item);

        item = new QuizItem();
        item.setQuestion("Leech, tapeworm, and tick are examples of __________.");
        item.setChoices(Arrays.asList("PREDATION", "PARASITE", "PARASITISM", "PREY"));
        item.setAcceptedAnswers(Collections.singletonList("PARASITE"));
        items.add(item);

        item = new QuizItem();
        item.setQuestion("Snake: frog: snake is the ________ : frog is the prey.");
        item.setChoices(Arrays.asList("PREDATOR", "PARASITE", "PARASITISM", "PREY"));
        item.setAcceptedAnswers(Collections.singletonList("PREDATOR"));
        items.add(item);

        item = new QuizItem();
        item.setQuestion("What is being transferred from the producer to consumer?");
        item.setChoices(Arrays.asList("COMMENSALISM", "ENERGY", "PARASITISM", "MUTUALISM"));
        item.setAcceptedAnswers(Collections.singletonList("ENERGY"));
        items.add(item);

        item = new QuizItem();
        item.setQuestion("Feeding relationship where both organisms are benefited by another.");
        item.setChoices(Arrays.asList("COMMENSALISM", "ENERGY", "PARASITISM", "MUTUALISM"));
        item.setAcceptedAnswers(Collections.singletonList("MUTUALISM"));
        items.add(item);

        item = new QuizItem();
        item.setQuestion("Relationship that exists between an animal that feeds on the animal where it lives.");
        item.setChoices(Arrays.asList("COMMENSALISM", "ENERGY", "PARASITISM", "MUTUALISM"));
        item.setAcceptedAnswers(Collections.singletonList("PARASITISM"));
        items.add(item);

        item = new QuizItem();
        item.setQuestion("Relationship between orchids in a branch of tree.");
        item.setChoices(Arrays.asList("COMMENSALISM", "COMPETITION", "PARASITISM", "MUTUALISM"));
        item.setAcceptedAnswers(Collections.singletonList("COMMENSALISM"));
        items.add(item);


        item = new QuizItem();
        item.setQuestion("  The animal being eaten or hunted for food.");
        item.setChoices(Arrays.asList("HOST", "COMPETITION", "PREY", "PARASITE"));
        item.setAcceptedAnswers(Collections.singletonList("PREY"));
        items.add(item);


        item = new QuizItem();
        item.setQuestion("When there are limited resources (food, water and space) there is ____________?");
        item.setChoices(Arrays.asList("HOST", "COMPETITION", "COMMENSALISM", "MUTUALISM"));
        item.setAcceptedAnswers(Collections.singletonList("COMPETITION"));
        items.add(item);

        item = new QuizItem();
        item.setQuestion("What is being transferred from the producer to consumer?");
        item.setChoices(Arrays.asList("COMMENSALISM", "ENERGY", "PARASITISM", "MUTUALISM"));
        item.setAcceptedAnswers(Collections.singletonList("ENERGY"));
        items.add(item);

        item = new QuizItem();
        item.setQuestion("Feeding relationship where both organisms are benefited by another.");
        item.setChoices(Arrays.asList("COMMENSALISM", "ENERGY", "PARASITISM", "MUTUALISM"));
        item.setAcceptedAnswers(Collections.singletonList("MUTUALISM"));
        items.add(item);

        Collections.shuffle(items);
        return new ArrayList<>(items.subList(0, size));
    }
}

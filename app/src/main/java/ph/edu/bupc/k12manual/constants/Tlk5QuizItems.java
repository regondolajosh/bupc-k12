package ph.edu.bupc.k12manual.constants;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

import ph.edu.bupc.k12manual.models.QuizItem;

public class Tlk5QuizItems {

    public static ArrayList<QuizItem> getQuizItems(int size){
        ArrayList<QuizItem> items = new ArrayList<>();

        QuizItem item = new QuizItem();
        item.setQuestion("What problems may arise in an overcrowded community?");
        item.setChoices(Arrays.asList("Food shortage", "Water shortage", "Poor health", "All of the above"));
        item.setAcceptedAnswers(Arrays.asList("All of the above"));
        items.add(item);

        item = new QuizItem();
        item.setQuestion("More people may mean more will use cars, buses and other vehicles. What will be the bad effect of this to our community?");
        item.setChoices(Arrays.asList("Water pollution", "Garbage problem", "Air pollution", "Soil erosion"));
        item.setAcceptedAnswers(Arrays.asList("Air pollution"));
        items.add(item);

        item = new QuizItem();
        item.setQuestion("In a place where the population is big, communicable diseases can be easily transferred from one person to another like sore eyes and simple colds. What harm does this show?");
        item.setChoices(Arrays.asList("Overpopulation can cause money problem", "Overpopulation means more doctors needed", "Overpopulation can cause health problems", "Overpopulation means more food needed"));
        item.setAcceptedAnswers(Arrays.asList("Overpopulation can cause health problems"));
        items.add(item);

        item = new QuizItem();
        item.setQuestion("Which of the following statements is true about overpopulation and its effects on the environment?");
        item.setChoices(Arrays.asList("As the population grows, more garbage is produced",
                "As population grows, more motor vehicle will emit poisonous exhaust gas",
                "As population increases in a small area, cholera, dysentery, and tuberculosis will spread",
                "All of the above"));
        item.setAcceptedAnswers(Arrays.asList("All of the above"));
        items.add(item);

        item = new QuizItem();
        item.setQuestion("When the number of people in a barangay increases, which of the following will also increase?");
        item.setChoices(Arrays.asList("Amount of soil", "Amount of rain", "Amount of sunlight", "Amount of garbage"));
        item.setAcceptedAnswers(Arrays.asList("Amount of garbage"));
        items.add(item);

        item = new QuizItem();
        item.setQuestion("How does rapid population growth can upset the ecological balance?");
        item.setChoices(Arrays.asList("Rapid population growth means more human power for the country",
                "Rapid population growth may mean more individuals that will plant trees",
                "Rapid population growth may mean more people will use limited natural resources which may result to destruction of our nature",
                "Rapid population growth may mean big budget must be given and allocated especially to the Department of Environment and Natural Resources."));
        item.setAcceptedAnswers(Arrays.asList("Rapid population growth may mean more people will use limited natural resources which may result to destruction of our nature."));
        items.add(item);

        item = new QuizItem();
        item.setQuestion("Which of the following problems would arise if the number of people in a place increases very fast?");
        item.setChoices(Arrays.asList("Not enough food for all people in the place",
                "Not enough water for all the people in the place",
                "Not enough space for houses for all people in the place",
                "All of the above"));
        item.setAcceptedAnswers(Arrays.asList("All of the above"));
        items.add(item);

        item = new QuizItem();
        item.setQuestion("Overpopulation can cause ecological imbalance that may pose harm to the environment.  More individuals may mean _________________________________.");
        item.setChoices(Arrays.asList("More wastes to be produced",
                "More theme parks will be built",
                "More shopping malls to be opened",
                "More schools to be put up by the government"));
        item.setAcceptedAnswers(Arrays.asList("More wastes to be produced"));
        items.add(item);

        item = new QuizItem();
        item.setQuestion("More people may mean more greenhouse gases to be produced like water vapor and carbon dioxide which will result to ________________________.");
        item.setChoices(Arrays.asList("Less greenhouse effect", "More greenhouse effect", "No greenhouse effect", "All of the above"));
        item.setAcceptedAnswers(Arrays.asList("More greenhouse effect"));
        items.add(item);

        item = new QuizItem();
        item.setQuestion("If trees will be cut continuously to build houses and make furniture for the growing population, what cycle will be affected that may cause too much greenhouse in the air?");
        item.setChoices(Arrays.asList("Life cycle", "Water cycle", "Oxygen-carbon dioxide cycle", "Land-air-water cycle"));
        item.setAcceptedAnswers(Arrays.asList("Oxygen-carbon dioxide cycle"));
        items.add(item);

        Collections.shuffle(items);

        return new ArrayList<>(items.subList(0, size));
    }
}

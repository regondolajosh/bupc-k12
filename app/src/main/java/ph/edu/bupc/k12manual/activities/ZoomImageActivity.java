package ph.edu.bupc.k12manual.activities;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;

import ph.edu.bulsu.k12manual.R;
import ph.edu.bupc.k12manual.constants.BundleIDs;

public class ZoomImageActivity extends AppCompatActivity{

    private ImageView ivZoom;
    private int viewId;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_zoom);
        viewId = getIntent().getIntExtra(BundleIDs.VIDEO_FILE, 0);
        ivZoom = (ImageView) findViewById(R.id.ivZoom);
        ivZoom.setImageResource(getDrawableId(viewId));
        findViewById(R.id.btnOK).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private int getDrawableId(int viewId) {
        switch (viewId) {
            case R.id.ivDeludedForest:
                return R.drawable.tlk3_forest;
            case R.id.ivPyramid1:
                return R.drawable.tlk1_pyramid1;
            case R.id.ivPyramid2:
                return R.drawable.tlk1_pyramid2;
            case R.id.ivMutualism:
                return R.drawable.tlk1_mutualism;
            case R.id.ivCommensalism:
                return R.drawable.tlk1_commensalism;
            case R.id.ivParasitism:
                return R.drawable.tlk1_parasitism;
            case R.id.ivPredation:
                return R.drawable.tlk1_predation;
            case R.id.ivCompetition:
                return R.drawable.tlk1_competition;
            case R.id.ivForest2:
                return R.drawable.tlk3_denuded_forest;
            case R.id.ivForest:
                return R.drawable.tlk4_forest;
            case R.id.ivLandslide:
                return R.drawable.tlk4_landslide;
            case R.id.ivAirpolution:
                return R.drawable.tlk4_air_polution;
            case R.id.ivFishkill:
                return R.drawable.tlk4_fish_kill;
            case R.id.ivFoodshortage:
                return R.drawable.tlk4_food_shortage;
            case R.id.ivFlood:
                return R.drawable.tlk4_flood;
            case R.id.ivWaterpolution:
                return R.drawable.tlk4_water_polution;
            case R.id.ivPopulation:
                return R.drawable.tlk5_population;
            case R.id.ivBurningforest:
                return R.drawable.tlk6_burning_forest;
            case R.id.ivCoral:
                return R.drawable.tlk6_coral;
            case R.id.ivClouds1:
                return R.drawable.tlk6_clouds_1;
            case R.id.ivGlobalwarmingg:
                return R.drawable.tlk4_global_warmingg;
            default:
                return 0;
        }
    }
}

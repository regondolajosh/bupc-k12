package ph.edu.bupc.k12manual.constants;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

import ph.edu.bupc.k12manual.models.QuizItem;


public class Tlk4QuizItems {

    public static ArrayList<QuizItem> getQuizItems(int size) {
        ArrayList<QuizItem> items = new ArrayList<>();

        QuizItem item = new QuizItem();
        item.setQuestion("To clear our forests, some farmers of our country are practicing bum-type of forest for agricultural purposes. Destruction of forest and wooded areas due to this wrong practice has deprived birds of their nesting places. Habitats of animals were destroyed and it endangered these wild animals. Soil erosion happened when higher places become denuded that may result to landslide.");
        item.setChoices(Arrays.asList("Mining", "Burning materials", "Kaingin system", "Oil spill", "Excessive use of fertilizers"));
        item.setAcceptedAnswers(Arrays.asList("Kaingin system"));
        items.add(item);

        item = new QuizItem();
        item.setQuestion("Because of the increase in human activities, greater amounts of fossil fuels like coal, oil and natural gas have been burned. Likewise, the use of wood for fuel and the burning of waste produces large amount of carbon dioxide in the air. This amount is more than what the plants and trees can use for the carbon cycle. The accumulation of carbon dioxide causes a rise in the earth’s temperature called global warming.");
        item.setChoices(Arrays.asList("Kaingin system", "Burning materials", "Excessive use of fertilizers", "Mining", "Deforestation"));
        item.setAcceptedAnswers(Arrays.asList("Burning materials"));
        items.add(item);

        item = new QuizItem();
        item.setQuestion("Farming activities affect the nitrogen cycle. When commercial fertilizers are over used, the crops do not use excess nutrients. Instead, these are washed away and carried off by rainfall into lakes and rivers. In these bodies of water, the abnormal growth of algae is promoted. When the algae die, they decompose, using up the dissolved oxygen in the water. Because of this, less oxygen is available for the fish and other aquatic organisms. The fishes die because the water has become too polluted. This is what we call fish kill.");
        item.setChoices(Arrays.asList("Dynamite fishing", "Kaingin system", "Burning materials", "Improper garbage disposal", "Excessive use of fertilizers"));
        item.setAcceptedAnswers(Arrays.asList("Excessive use of fertilizers"));
        items.add(item);

        item = new QuizItem();
        item.setQuestion("Oil thrown in seas and oceans, whether accidental or planned, cover the water surface. This prevents sunlight from reaching the algae. So, the algae cannot make their food and eventually die. When the water is covered with oil and chemicals, fish and other aquatic animals cannot breathe in it. They will not be able to get their food and they will die.");
        item.setChoices(Arrays.asList("Improper garbage disposal", "Excessive use of fertilizers", "Deforestation", "Oil spill", "Mining"));
        item.setAcceptedAnswers(Arrays.asList("Oil spill"));
        items.add(item);

        item = new QuizItem();
        item.setQuestion("Getting valuable minerals and metals like diamond and gold causes harm when the fertile topsoil is removed. It is then replaced by waste soil that is contaminated with poisonous chemicals used in mining. The microorganisms in the soil are not able to break down the poison. Instead, they die, and the place becomes totally barren and unproductive.");
        item.setChoices(Arrays.asList("Mining", "Deforestation", "Oil spill", "Kaingin system", "Burning materials"));
        item.setAcceptedAnswers(Arrays.asList("Mining"));
        items.add(item);

        item = new QuizItem();
        item.setQuestion("Wastes and substances are continuously thrown in rivers and bodies of water. All kinds of garbage wastes are washed down directly into rivers. Factories release chemicals, acids, and dye. These substances contaminate water. Decay sets in and the foul odor is released into the air. The water pollutants become air pollutants, too.");
        item.setChoices(Arrays.asList("Oil spill", "Mining", "Improper garbage disposal", "Deforestation", "Kaingin system"));
        item.setAcceptedAnswers(Arrays.asList("Improper garbage disposal"));
        items.add(item);

        item = new QuizItem();
        item.setQuestion("Forest trees are cut down by loggers for profit. They do not properly replant and don’t take care of the young trees which are supposed to replace the cut trees. Large forest areas are slowly being converted to agricultural lands and subdivisions. As trees are cut, the habitats of animals are destroyed. Many species of animals and plants die and be endangered. Less trees also affects the oxygen-carbon dioxide cycle.");
        item.setChoices(Arrays.asList("Burning materials", "Improper garbage disposal", "Dynamite fishing", "Deforestation", "Oil spill"));
        item.setAcceptedAnswers(Arrays.asList("Deforestation"));
        items.add(item);

        item = new QuizItem();
        item.setQuestion("Some fishermen want to catch more fish easier and faster by using explosives. This is not allowed, but they still do it. Small or young fish unfit for human consumption are being killed. The fish population is wasted and decreases because it also destroys the natural habitat of marine life, the coral reefs. It is also dangerous for fishermen too.");
        item.setChoices(Arrays.asList("Deforestation", "Kaingin system", "Mining", "Excessive use of fertilizers", "Dynamite fishing"));
        item.setAcceptedAnswers(Arrays.asList("Dynamite fishing"));
        items.add(item);

        item = new QuizItem();
        item.setQuestion("  The animal being eaten or hunted for food.");
        item.setChoices(Arrays.asList("HOST", "COMPETITION", "PREY", "PARASITE"));
        item.setAcceptedAnswers(Collections.singletonList("PREY"));
        items.add(item);


        item = new QuizItem();
        item.setQuestion("When there are limited resources (food, water and space) there is ____________?");
        item.setChoices(Arrays.asList("HOST", "COMPETITION", "COMMENSALISM", "MUTUALISM"));
        item.setAcceptedAnswers(Collections.singletonList("COMPETITION"));
        items.add(item);


        Collections.shuffle(items);
        return new ArrayList<>(items.subList(0, size));
    }
}

package ph.edu.bupc.k12manual.constants;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

import ph.edu.bupc.k12manual.models.QuizItem;


public class Tlk3QuizItems {
    public static ArrayList<QuizItem> getQuizItems(int size) {
        ArrayList<QuizItem> items = new ArrayList<>();

        QuizItem item = new QuizItem();
        item.setQuestion("Philippines is blessed with forests. We can find species of plants and animals that are only found in our country. What is the importance of forest to the animals?");
        item.setChoices(Arrays.asList("Forest provides them happiness",
                "Forest provides them their playmates",
                "Forest provides them their natural habitat,",
                "Forest removes their predators"));
        item.setAcceptedAnswers(Arrays.asList("Forest provides them their natural habitat."));
        items.add(item);

        item = new QuizItem();
        item.setQuestion("What events in our environment can be avoided with the presence of healthy forests?        \nI. Floods      \nII. Lightning       \nIII. Landslides");
        item.setChoices(Arrays.asList("I and II", "I and III", "II and III", "I, II and III"));
        item.setAcceptedAnswers(Arrays.asList("I and III"));
        items.add(item);

        item = new QuizItem();
        item.setQuestion("What is the importance role of forest in preventing flash flood in the plains?\n");
        item.setChoices(Arrays.asList("Forest prevents rain from falling",
                "Forest absorbs excess rain water",
                "Forest prevents frequent occurrence of rain",
                "Forest makes the environment drier or more humid"));
        item.setAcceptedAnswers(Arrays.asList("Forest absorbs excess rain water."));
        items.add(item);

        item = new QuizItem();
        item.setQuestion("Continuous cutting of trees can harm our forests. What will be the effect of deforestation?");
        item.setChoices(Arrays.asList("more plants will grow",
                "more animals can reproduce",
                "more oxygen will be produced",
                "more animals will lose their habitat"));
        item.setAcceptedAnswers(Arrays.asList("more animals will lose their habitat"));
        items.add(item);

        item = new QuizItem();
        item.setQuestion("Less trees in the forest is harmful in our ecosystem. How can people be affected by deforestation?");
        item.setChoices(Arrays.asList("there will be less oxygen in the atmosphere",
                "there will be frequent occurrence of land slides",
                "there will be less supply of raw materials to factories",
                "all of the above"));
        item.setAcceptedAnswers(Arrays.asList("all of the above"));
        items.add(item);

        item = new QuizItem();
        item.setQuestion("What will be the instant effect of deforestation to the food chains and food webs?");
        item.setChoices(Arrays.asList("Animals will all die instantly because of less oxygen in the atmosphere",
                "Animals that are primary consumers will find it hard to find food to eat",
                "Animals will all get weak because they are going to be tired in looking for food",
                "Animals will all get sick because they cannot find medicines to cure their illness"));
        item.setAcceptedAnswers(Arrays.asList("Animals that are primary consumers will find it hard to find food to eat."));
        items.add(item);

        item = new QuizItem();
        item.setQuestion("What greenhouse gas will increase the amount in our atmosphere because of deforestation?");
        item.setChoices(Arrays.asList("Deforestation", "Watershed", "Endangered species", "Extinct species"));
        item.setAcceptedAnswers(Arrays.asList("Extinct species"));
        items.add(item);

        item = new QuizItem();
        item.setQuestion("What will be the effect to our atmospheric temperature if there’s more carbon dioxide in the atmosphere?");
        item.setChoices(Arrays.asList("temperature will increase and we will feel colder",
                "temperature will decrease and we will feel colder",
                "temperature will increase and we will feel warmer",
                "temperature will decrease and we will feel warmer"));
        item.setAcceptedAnswers(Arrays.asList("temperature will increase and we will feel warmer"));
        items.add(item);

        item = new QuizItem();
        item.setQuestion("More carbon dioxide, water vapor, nitrous oxide and other gases can cause warmer temperature in our planet. This is phenomenon is called _____________.");
        item.setChoices(Arrays.asList("water cycle", "rotation", "cause and effect", "greenhouse effect"));
        item.setAcceptedAnswers(Arrays.asList("greenhouse effect"));
        items.add(item);

        item = new QuizItem();
        item.setQuestion("In the long run, if there will be less trees and less forest areas around the world all the people and other living things around the world will be affected because of __________.");
        item.setChoices(Arrays.asList("physical change", "chemical change", "climate change", "no answer"));
        item.setAcceptedAnswers(Arrays.asList("climate change"));
        items.add(item);

        item = new QuizItem();
        item.setQuestion("The natural dwelling place of an animal or plant. Animals live in a forest because it is their home.");
        item.setChoices(Arrays.asList("home", "habitat", "ecosystem", "forest"));
        item.setAcceptedAnswers(Arrays.asList("habitat"));
        items.add(item);

        item = new QuizItem();
        item.setQuestion("A forest is a good source of good woods. Narra, apitong, tanguige and guijo provide good construction materials. These are the types of hardwood that are best used for ceiling, furniture and building houses. ");
        item.setChoices(Arrays.asList("lumber", "rubber", "paper", "minerals"));
        item.setAcceptedAnswers(Arrays.asList("lumber"));
        items.add(item);


        item = new QuizItem();
        item.setQuestion(" Trees like pulpwood can be turned to products such as books, newsprint, cardboards, writing pads, illustration boards and notebooks.");
        item.setChoices(Arrays.asList("pencil", "rubber", "paper", "minerals"));
        item.setAcceptedAnswers(Arrays.asList("paper"));
        items.add(item);

        item = new QuizItem();
        item.setQuestion("Scientist discovered substances that come from plants which can be used to treat diseases. Few examples are taheebo, cocoa tree, sambong and etc. These plants are plenty in the forests.");
        item.setChoices(Arrays.asList("woods", "herbal medicine", "flowers", "minerals"));
        item.setAcceptedAnswers(Arrays.asList("herbal medicine"));
        items.add(item);

        item = new QuizItem();
        item.setQuestion("Animals need this gas to breathe and forest provides this to us because there are trees and plants to be found in a forest ecosystem. ");
        item.setChoices(Arrays.asList("carbon dioxide", "nitrogen", "water", "oxygen"));
        item.setAcceptedAnswers(Arrays.asList("oxygen"));
        items.add(item);

        Collections.shuffle(items);
        return new ArrayList<>(items.subList(0, size));
    }
}

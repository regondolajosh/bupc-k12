package ph.edu.bupc.k12manual.activities;

import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.VideoView;

import ph.edu.bulsu.k12manual.R;
import ph.edu.bupc.k12manual.constants.BundleIDs;


public class SimulationActivity extends AppCompatActivity {

    private VideoView videoView;
    private int viewId;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_simulation);
        viewId = getIntent().getIntExtra(BundleIDs.VIDEO_FILE, 0);
        videoView = (VideoView) findViewById(R.id.vvSimulation);
        videoView.setVideoURI(Uri.parse(getPath(viewId)));
        videoView.start();

        findViewById(R.id.btnOK).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private String getPath(int viewId) {
        String path ="";

        switch (viewId) {
            case R.id.ivNitrogenCycle:
                path = "android.resource://" + getPackageName() + "/" + "raw/"+ R.raw.nitrogen_cycle;
                break;
            case R.id.ivEarthsSurface:
                path = "android.resource://" + getPackageName() + "/" + "raw/"+ R.raw.earth_39_s_surface;
                break;
            case R.id.ivGreenHouseEffect:
                path = "android.resource://" + getPackageName() + "/" + "raw/"+ R.raw.greenhouse_effect;
                break;
            case R.id.ivTheGreenHouseEffect:
                path = "android.resource://" + getPackageName() + "/" + "raw/"+ R.raw.the_greenhouse_effect;
                break;
            case R.id.ivEarthGreenhouse:
                path = "android.resource://" + getPackageName() + "/" + "raw/"+ R.raw.earth_greenhouse;
                break;
            case R.id.ivTyphoon:
                path = "android.resource://" + getPackageName() + "/" + "raw/"+ R.raw.typhoon;
                break;
            case R.id.ivWatershed:
                path = "android.resource://" + getPackageName() + "/" + "raw/"+ R.raw.watershed;
                break;
            case R.id.ivFoodchain:
                path = "android.resource://" + getPackageName() + "/" + "raw/"+ R.raw.foodchain;
                break;
            case R.id.ivOxygencarbondioxidecycle:
                path = "android.resource://" + getPackageName() + "/" + "raw/"+ R.raw.oxygencarbondioxidecycle;
                break;
            case R.id.ivWatercycle:
                path = "android.resource://" + getPackageName() + "/" + "raw/"+ R.raw.watercycle;
                break;
            case R.id.ivNowater:
                path = "android.resource://" + getPackageName() + "/" + "raw/"+ R.raw.nowater;
                break;
            case R.id.ivNight:
                path = "android.resource://" + getPackageName() + "/" + "raw/"+ R.raw.night;
                break;
            case R.id.ivDay:
                path = "android.resource://" + getPackageName() + "/" + "raw/"+ R.raw.day;
                break;
            case R.id.ivAnimals:
                path = "android.resource://" + getPackageName() + "/" + "raw/"+ R.raw.animals;
                break;
            case R.id.ivHouse:
                path = "android.resource://" + getPackageName() + "/" + "raw/"+ R.raw.toomuch;
                break;
        }


        return path;
    }
}

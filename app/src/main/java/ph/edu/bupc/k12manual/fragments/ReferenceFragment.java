package ph.edu.bupc.k12manual.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import ph.edu.bulsu.k12manual.R;


public class ReferenceFragment extends Fragment {

    private final String TAG = ReferenceFragment.class.getSimpleName();
    private View mView;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_reference, container, false);

        return mView;
    }

    public static ReferenceFragment newInstance() {
        
        Bundle args = new Bundle();
        
        ReferenceFragment fragment = new ReferenceFragment();
        fragment.setArguments(args);
        return fragment;
    }
}
